import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoxTest {
    // Run tests against getters
    // Run test that if a box type is entered, it will run.
    @Test
    void getBoxType() {
        Box b = new Box("Open Box", 50.0);
        assertEquals("Open Box", b.getBoxType());
    }
    // Run test that if a volume is entered, it will run.
    @Test
    void getVolume() {
        Box b = new Box("Open Box", 50.0);
        assertEquals(50, b.getVolume(), 0.001); // Used a delta for fine-tuning
    }
    // Run tests against Setters

    // Run Test that the box type will work for the setter
    @Test
    void setBoxType() {
        Box b = new Box("Open Box", 50.0);
        b.setBoxType("Open Box");
        assertEquals("Open Box", b.getBoxType());
    }

    @Test
    void setVolume() {
        Box b = new Box("Open Box", 50.0);
        b.setBoxType("Open Box");
        assertEquals(50, b.getVolume(), 0.001);
    }
    // Make sure that negative numbers do not exist.
    @Test
    void testNegativesThrows() {
        Box box = new Box("Open Box", 50.0);
        assertThrows(IllegalArgumentException.class,
                () -> box.setVolume(-1.25)
                );
    }

// End tests Week 5

}