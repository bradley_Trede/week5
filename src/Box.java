import java.sql.SQLOutput;

/* Code assumes a box of products and the respective volume
   and that the contents of the box are within various perimeters
   within testing of the code
 */
public class Box {
    public double volume;
    public String boxType;

// Set Vars
    public Box(String boxType, double volume){
            this.boxType = boxType;
            this.volume = volume;
        }
// Getters for Box Type (Open, Closed Semi-full, etc.)
        public String getBoxType () {

            return boxType;
        }
// Setters for Box Type
        public void setBoxType (String boxType){

            this.boxType = boxType;
        }
// Getter for the volume of the box.
        public Double getVolume () {

            return volume;
        }
// Setter for the volume
        public void setVolume (Double volume){

            this.volume = volume;
        }


        public void setVolumeNotNegative ( double volume){
            if (volume < 0) {
                throw new IllegalArgumentException("Volume cannot be negative");
            }
            this.volume = volume;
        }
    }



